package com.lachlanvass.materialnotes.domain

import com.lachlanvass.materialnotes.domain.model.Note
import kotlinx.coroutines.flow.Flow

interface NoteRepositoryInterface {

    fun getNotes(): Flow<List<Note>>
    suspend fun insertNote(note: Note)
    suspend fun deleteNote(note: Note)
}