package com.lachlanvass.materialnotes.data.repository

import com.lachlanvass.materialnotes.data.NoteDao
import com.lachlanvass.materialnotes.domain.NoteRepositoryInterface
import com.lachlanvass.materialnotes.domain.model.Note
import kotlinx.coroutines.flow.Flow

class NoteRepository(private val notedao: NoteDao): NoteRepositoryInterface {
    override fun getNotes(): Flow<List<Note>> {
        return notedao.getNotes()
    }

    override suspend fun insertNote(note: Note) {
        notedao.insertNote(note)
    }

    override suspend fun deleteNote(note: Note) {
        notedao.deleteNote(note)
    }


}