package com.lachlanvass.materialnotes

import android.content.Context
import androidx.room.Room
import com.lachlanvass.materialnotes.data.NoteDatabase

object AppModule {

    fun getNotesDatabase(context: Context): NoteDatabase =
        Room
            .databaseBuilder(context, NoteDatabase::class.java, NoteDatabase.DATABASE_NAME)
            .build()
}