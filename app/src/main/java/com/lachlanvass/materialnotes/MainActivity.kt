package com.lachlanvass.materialnotes

import android.os.Bundle
import android.provider.ContactsContract
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.lifecycle.ViewModel
import com.lachlanvass.materialnotes.domain.model.Note
import com.lachlanvass.materialnotes.presentation.notes.MainScreenNote
import com.lachlanvass.materialnotes.presentation.notes.NotesViewModel
import com.lachlanvass.materialnotes.ui.theme.MaterialNotesTheme
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.forEach
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val model: NotesViewModel by viewModels()

        var notes = listOf<Note>()
        runBlocking {

            notes = model.getNotes()
        }

        setContent {
            MaterialNotesTheme {

                notes.forEach { note -> MainScreenNote(note) }

            }
        }
    }
}