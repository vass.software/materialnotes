package com.lachlanvass.materialnotes.presentation.notes

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import com.lachlanvass.materialnotes.domain.model.Note

@Composable
fun MainScreenNote(
    note: Note
) {

    Box {
        Row { Text(text = note.title) }
    }
}