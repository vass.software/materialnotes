package com.lachlanvass.materialnotes.presentation.notes

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lachlanvass.materialnotes.AppModule
import com.lachlanvass.materialnotes.data.repository.NoteRepository
import com.lachlanvass.materialnotes.domain.NoteRepositoryInterface
import com.lachlanvass.materialnotes.domain.model.Note
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.newCoroutineContext

class NotesViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: NoteRepositoryInterface

    init {
        val notedb = AppModule.getNotesDatabase(application)
        val notedao = notedb.noteDao
        repository = NoteRepository(notedao)

    }

    suspend fun getNotes(): List<Note> {

        return repository.getNotes().flatMapConcat {
            it.asFlow()
        }.toList()
    }

}